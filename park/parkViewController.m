//
//  parkViewController.m
//  park
//
//  Created by Ankith Konda on 13/07/2014.
//  Copyright (c) 2014 Ankith Konda. All rights reserved.
//

#import "parkViewController.h"
#import "UIImageView+AFNetworking.h"

@interface parkViewController ()<UITableViewDataSource, UITableViewDelegate, NSXMLParserDelegate>


@property (strong, nonatomic) UILabel *whatWouldYouLike;


@property BOOL swimPressed;
@property BOOL dogPressed;
@property BOOL PicnicPressed;
@property BOOL BBQPressed;
@property BOOL activePressed;
@property BOOL sportPressed;
@property BOOL workoutPressed;
@property BOOL skatePressed;







@end

@implementation parkViewController

@synthesize titleImageView;

@synthesize contentView;

@synthesize leftButtonView;
@synthesize rightButtonView;
@synthesize parksButton;
@synthesize eventsButton;
@synthesize whatWouldYouLike;



@synthesize swimPressed;
@synthesize dogPressed;
@synthesize PicnicPressed;
@synthesize BBQPressed;
@synthesize activePressed;
@synthesize sportPressed;
@synthesize workoutPressed;
@synthesize skatePressed;


@synthesize swimButton;
@synthesize dogButton;
@synthesize picnicButton;
@synthesize bbqButton;
@synthesize activeButton;
@synthesize sportButton;
@synthesize workoutButton;
@synthesize skateButton;

@synthesize backButton;

@synthesize rssFeed;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"hello world");
    
    self.rssFeed = [[NSMutableArray alloc] init];
    
    self.swimPressed = NO;
    self.dogPressed = NO;
    self.PicnicPressed = NO;
    self.BBQPressed = NO;
    self.activePressed = NO;
    self.sportPressed = NO;
    self.workoutPressed = NO;
    self.skatePressed = NO;
    
    
    self.whatWouldYouLike = [[UILabel alloc] initWithFrame:CGRectMake((self.contentView.frame.size.width/2)-175, 100, 350, 35)];
    
    
    
    feeds = [[NSMutableArray alloc] init];
    NSURL *url = [NSURL URLWithString:@"http://www.trumba.com/calendars/brisbane-events-rss.rss?filterview=parks"];
    parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
    
    
    
    //load rss feed
    
    
    
    /**
     
     eventDate = "Sunday, August 31, 2014, 1 - 2:30pm\n\t\t\t";
     
     eventImage = "http://www.trumba.com/i/DgCb8mkBqHe5gaPppire6Ima.jpg";
     
     link = "http://www.trumba.com/calendars/brisbane-events-rss?eventid=109180761\n\t\t\t";
     
     title = "Kayak eco adventure\n\t\t\t";
     
     xcalDescription = "Have fun paddling as a team in two seat kayaks as you embark on one of the best ways to observe Brisbane's hidden beauty. The guided kayaking adventures are a great way to learn more about the local flora and wildlife of Brisbane waterways as well as having an experienced guide to help with kayaking skills. All associated kayaking and safety equipment is supplied as well as light refreshments.\n \n This event is for people aged 50 and over.\n\t\t\t";
     
     xcalLocation = "Jindalee Boat Ramp Park\n\t\t\t";
     
     */
    
    
}


-(void)viewDidAppear:(BOOL)animated{


[UIView animateWithDuration:2.0 animations:^{
    [self.titleImageView setFrame:CGRectMake(379, 40, self.titleImageView.frame.size.width, self.titleImageView.frame.size.height)];

} completion:^(BOOL finished) {
    
    [self startLoadingCrap];
    
}];

}


-(void)startLoadingCrap{

    
    
UIColor *aColor = [UIColor colorWithRed:0.118 green:0.783 blue:0.776 alpha:1.000];
    
    
    [self.whatWouldYouLike setBackgroundColor:aColor];
    [self.whatWouldYouLike setTextColor:[UIColor whiteColor]];
    [self.whatWouldYouLike setTextAlignment:NSTextAlignmentCenter];
    [self.whatWouldYouLike setFont:[UIFont fontWithName:@"verdana" size:20]];
    [self.whatWouldYouLike setText:@"What would you like to do today?"];
    [self.whatWouldYouLike.layer setMasksToBounds:YES];
    [self.whatWouldYouLike.layer setCornerRadius:5.0];
    
    [self.whatWouldYouLike setAlpha:0];

    
    
   // [self.contentView addSubview:welcomeMessage];
    [self.contentView addSubview:self.whatWouldYouLike];
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        [self.whatWouldYouLike setAlpha:1.0];
        
    } completion:^(BOOL finished) {
        
        [self bringForthButtons];
        
    }];
    

}

-(void)bringForthButtons{
   
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.leftButtonView.frame = CGRectMake(171, 176, self.leftButtonView.frame.size.width, self.leftButtonView.frame.size.height);
        
        self.rightButtonView.frame = CGRectMake(604, 176, self.rightButtonView.frame.size.width, self.rightButtonView.frame.size.height);
        
        [self.whatWouldYouLike setAlpha:1];
        [self.backButton setAlpha:0];
        
        
    } completion:^(BOOL finished) {
        
        
    }];

}

-(void)takeawayButtons{
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.leftButtonView.frame = CGRectMake(-250, 176, self.leftButtonView.frame.size.width, self.leftButtonView.frame.size.height);
        
        self.rightButtonView.frame = CGRectMake(1024, 176, self.rightButtonView.frame.size.width, self.rightButtonView.frame.size.height);
        
        [self.whatWouldYouLike setAlpha:0];
        [self.backButton setAlpha:1];


        
    } completion:^(BOOL finished) {
        
        
        
    }];



}




- (IBAction)eventsPressed:(id)sender {
    
    [self takeawayButtons];
    
    [self showEventsView];
    
    
}





- (IBAction)parksPressed:(id)sender {
    
    [self takeawayButtons];
    [self showParksView];
}


-(void)showEventsView{

    [UIView animateWithDuration:2.0 animations:^{
        
        self.eventsView.frame = CGRectMake(0, 0, self.eventsView.frame.size.width, self.eventsView.frame.size.height);
        
        
        
        
    } completion:^(BOOL finished) {
        
        
        
    }];



}

-(void)showParksView{
    
    [UIView animateWithDuration:2.0 animations:^{
        
        self.parksView.frame = CGRectMake(0, 0, self.parksView.frame.size.width, self.parksView.frame.size.height);
        
        
        
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}

-(void)hideEventsView{
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.eventsView.frame = CGRectMake(0, 583, self.eventsView.frame.size.width, self.eventsView.frame.size.height);
        
        
        
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}

-(void)hideParksView{
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.parksView.frame = CGRectMake(0, 583, self.parksView.frame.size.width, self.parksView.frame.size.height);
        
        
        
        
    } completion:^(BOOL finished) {
        
        
        
    }];
    
    
    
}



- (IBAction)backButton:(id)sender {
    
    
    [self hideEventsView];
    [self hideParksView];
    [self bringForthButtons];
    
    
}




-(void)removeSelectedImage:(UIButton *)button{
    
    for(UIView *view in [button subviews]){
        
        if(view.tag == 100){
            [view removeFromSuperview];
        }
    }
}

- (void)addSelectedImage:(UIButton *)button{
    
    UIImageView *selectedBlue = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selectionRing" ]];
    
    [selectedBlue setFrame:CGRectMake(0, 0, 100, 100)];
    
    //[selectedBlue setImage:[UIImage imageNamed:@"selectionRing" ]];
    
    [selectedBlue setTag:100];
    
    [button addSubview:selectedBlue];
    
    
    switch (button.tag)
    {
        case 11:
            self.swimPressed = YES;
            
            break;
        case 12:
            self.dogPressed = YES;
            break;
        case 13:
            self.PicnicPressed = YES;
            break;
        case 14:
            self.BBQPressed = YES;
            break;
        case 15:
            self.activePressed = YES;
            break;
        case 16:
            self.sportPressed = YES;
            break;
        case 17:
            self.workoutPressed = YES;
            break;
        case 18:
            self.skatePressed = YES;
            break;
        default:
            NSLog (@"Integer out of range");
            break;
    }
    
}



- (IBAction)swimmingButton:(id)sender {
    

    
    if (!self.swimPressed) {
        [self addSelectedImage:self.swimButton];
    }else{
        [self removeSelectedImage:self.swimButton];
        
    
    }
    
    
}


- (IBAction)walkTheDogButton:(id)sender {
    if (!self.dogPressed) {
        [self addSelectedImage:self.dogButton];
    }else{
        [self removeSelectedImage:self.dogButton];
        
    }
    
}


- (IBAction)picnicButton:(id)sender {
    if (!self.PicnicPressed) {
        [self addSelectedImage:self.picnicButton];
    }else{
        [self removeSelectedImage:self.picnicButton];
        
    }
    
}

- (IBAction)bbqButton:(id)sender {
    if (!self.BBQPressed) {
        [self addSelectedImage:self.bbqButton];
    }else{
        [self removeSelectedImage:self.bbqButton];
        
    }
    
}

- (IBAction)activeButton:(id)sender {
    if (!self.activePressed) {
        [self addSelectedImage:self.activeButton];
    }else{
        [self removeSelectedImage:self.activeButton];
        
    }
    
}

- (IBAction)sportButton:(id)sender {
    if (!self.sportPressed) {
        [self addSelectedImage:self.sportButton];
    }else{
        [self removeSelectedImage:self.sportButton];
        
    }
    
}

- (IBAction)workoutButton:(id)sender {
    if (!self.workoutPressed) {
        [self addSelectedImage:self.workoutButton];
    }else{
        [self removeSelectedImage:self.workoutButton];
        
    }
    
}
- (IBAction)skateButton:(id)sender {
    if (!self.skatePressed) {
        [self addSelectedImage:self.skateButton];
    }else{
        [self removeSelectedImage:self.skateButton];
        
    }
    
}


/** TABLE VIEW CRAP **/


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *MyIdentifier = @"eventCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    UIImageView *image = (UIImageView *)[cell viewWithTag:1];

    UILabel *titleLabel = (UILabel *)[cell viewWithTag:2];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *locationlabel = (UILabel *)[cell viewWithTag:4];
    UITextField *des = (UITextField *)[cell viewWithTag:5];

    NSMutableDictionary *itemToLoad = [self.rssFeed objectAtIndex:[indexPath row]];
    
    [titleLabel setText:[itemToLoad objectForKey:@"title"]];
    [dateLabel setText:[itemToLoad objectForKey:@"date"]];
    [locationlabel setText:[itemToLoad objectForKey:@"location"]];
    [des setText:[itemToLoad objectForKey:@"desc"]];
    
    NSURL *imageURL = [[NSURL alloc] initWithString:[itemToLoad objectForKey:@"image"]];
    
    [image setImageWithURL:imageURL];
    

    
    
    return cell;
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [self.rssFeed count];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    NSMutableDictionary *itemToLoad = [self.rssFeed objectAtIndex:[indexPath row]];
    
    NSURL *url = [ [ NSURL alloc ] initWithString: [itemToLoad objectForKey:@"link"] ];
    [[UIApplication sharedApplication] openURL:url];
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Parser

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        description = [[NSMutableString alloc] init];
        xcalDescription = [[NSMutableString alloc] init];
        xcalLocation = [[NSMutableString alloc] init];
        eventAddress   = [[NSMutableString alloc] init];
        eventDate = [[NSMutableString alloc] init];
        eventImage = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    } else if ([element isEqualToString:@"description"]) {
        [description appendString:string];
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    } else if ([element isEqualToString:@"xCal:description"]) {
        [xcalDescription appendString:string];
    } else if ([element isEqualToString:@"xCal:location"]) {
        [xcalLocation appendString:string];
    } else if ([element isEqualToString:@"x-trumba:formatteddatetime"]) {
        [eventDate appendString:string];
    } else if ([element isEqualToString:@"x-trumba:customfield"]) {
        // we only want the venue address but we can't get just it
        [eventAddress appendString:string];
        
        if ([string rangeOfString:@".jpg"].location != NSNotFound) {
            //NSLog(@"string:%@",string);
            // we found the image
            [eventImage appendString:string];
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:title forKey:@"title"];
        [item setObject:description forKey:@"description"];
        [item setObject:link forKey:@"link"];
        [item setObject:xcalDescription forKey:@"xcalDescription"];
        [item setObject:xcalLocation forKey:@"xcalLocation"];
        [item setObject:eventAddress forKey:@"eventAddress"];
        [item setObject:eventDate forKey:@"eventDate"];
        [item setObject:eventImage forKey:@"eventImage"];
        
        [feeds addObject:[item copy]];
    }
    
}


-(void)parserDidEndDocument:(NSXMLParser *)parser {

    
    /**
     
     eventDate = "Sunday, August 31, 2014, 1 - 2:30pm\n\t\t\t";
     
     eventImage = "http://www.trumba.com/i/DgCb8mkBqHe5gaPppire6Ima.jpg";
     
     link = "http://www.trumba.com/calendars/brisbane-events-rss?eventid=109180761\n\t\t\t";
     
     title = "Kayak eco adventure\n\t\t\t";
     
     xcalDescription = "Have fun paddling as a team in two seat kayaks as you embark on one of the best ways to observe Brisbane's hidden beauty. The guided kayaking adventures are a great way to learn more about the local flora and wildlife of Brisbane waterways as well as having an experienced guide to help with kayaking skills. All associated kayaking and safety equipment is supplied as well as light refreshments.\n \n This event is for people aged 50 and over.\n\t\t\t";
     
     xcalLocation = "Jindalee Boat Ramp Park\n\t\t\t";
     
     */
    
    
    for (NSMutableDictionary *myItem in feeds) {
        
        NSMutableDictionary *newItem = [[NSMutableDictionary alloc] init];
        
        NSString *preTitle = [myItem objectForKey:@"title"];
        
        NSString *updatedTitle = [preTitle stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *myTitle = [updatedTitle stringByReplacingOccurrencesOfString:@"\t" withString:@""];

        
        NSString *preDate = [myItem objectForKey:@"eventDate"];
        
        NSString *updatedDate = [preDate stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *myDate = [updatedDate stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        
        
        NSString *preLink = [myItem objectForKey:@"link"];
        
        NSString *updatedLink = [preLink stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *myLink = [updatedLink stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        
        NSString *preLocation = [myItem objectForKey:@"xcalLocation"];
        
        NSString *updatedLocation = [preLocation stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *myLocation = [updatedLocation stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        
        NSString *preDesc = [myItem objectForKey:@"xcalDescription"];
        
        NSString *updatedDesc = [preDesc stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        NSString *myDesc = [updatedDesc stringByReplacingOccurrencesOfString:@"\t" withString:@""];
        
        
        NSString *imageURL = [myItem objectForKey:@"eventImage"];
        
        
        [newItem setObject:myTitle forKey:@"title"];
        [newItem setObject:myDate forKey:@"date"];
        [newItem setObject:myLink forKey:@"link"];
        [newItem setObject:myLocation forKey:@"location"];
        [newItem setObject:myDesc forKey:@"desc"];
        [newItem setObject:imageURL forKey:@"image"];
        
        
        [self.rssFeed addObject:newItem];
    }

    
    NSLog(@"%@", self.rssFeed);
}

- (IBAction)goToMap:(id)sender {
    [self performSegueWithIdentifier:@"pushMap" sender:nil];
    
}


@end
