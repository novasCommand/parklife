//
//  parkAppDelegate.h
//  park
//
//  Created by Ankith Konda on 13/07/2014.
//  Copyright (c) 2014 Ankith Konda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface parkAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
