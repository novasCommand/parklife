//
//  parkViewController.h
//  park
//
//  Created by Ankith Konda on 13/07/2014.
//  Copyright (c) 2014 Ankith Konda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSParser.h"
#import "RSSItem.h"


@interface parkViewController : UIViewController <NSXMLParserDelegate> {
    NSXMLParser *parser;
    NSMutableArray *feeds;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *description;
    NSMutableString *xcalDescription;
    NSMutableString *xcalLocation;
    NSMutableString *eventDate;
    NSMutableString *eventAddress;
    NSMutableString *link;
    NSMutableString *eventImage;
    
    NSString *element;
    long counter;
}


@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;

@property (weak, nonatomic) IBOutlet UIView *contentView;


@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *parksButton;

@property (weak, nonatomic) IBOutlet UIView *leftButtonView;
@property (weak, nonatomic) IBOutlet UIView *rightButtonView;

@property (weak, nonatomic) IBOutlet UIView *eventsTableTitle;


@property (weak, nonatomic) IBOutlet UITableView *eventsTableView;






@property (weak, nonatomic) IBOutlet UIButton *swimButton;

@property (weak, nonatomic) IBOutlet UIButton *dogButton;

@property (weak, nonatomic) IBOutlet UIButton *picnicButton;

@property (weak, nonatomic) IBOutlet UIButton *bbqButton;


@property (weak, nonatomic) IBOutlet UIButton *activeButton;

@property (weak, nonatomic) IBOutlet UIButton *sportButton;

@property (weak, nonatomic) IBOutlet UIButton *workoutButton;

@property (weak, nonatomic) IBOutlet UIButton *skateButton;




@property (weak, nonatomic) IBOutlet UILabel *swimTitle;
@property (weak, nonatomic) IBOutlet UILabel *dogTitle;
@property (weak, nonatomic) IBOutlet UILabel *picnicTitle;
@property (weak, nonatomic) IBOutlet UILabel *bbqTitle;
@property (weak, nonatomic) IBOutlet UILabel *activeTitle;
@property (weak, nonatomic) IBOutlet UILabel *sportTitle;
@property (weak, nonatomic) IBOutlet UILabel *workoutTitle;
@property (weak, nonatomic) IBOutlet UILabel *skateTitle;


@property (weak, nonatomic) IBOutlet UIView *eventsView;
@property (weak, nonatomic) IBOutlet UIView *parksView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (strong, nonatomic) NSMutableArray *rssFeed;

@end
