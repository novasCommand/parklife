//
//  main.m
//  park
//
//  Created by Ankith Konda on 13/07/2014.
//  Copyright (c) 2014 Ankith Konda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "parkAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([parkAppDelegate class]));
    }
}
